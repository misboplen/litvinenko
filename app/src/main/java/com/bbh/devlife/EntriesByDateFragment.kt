package com.bbh.devlife

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class EntriesByDateFragment : Fragment() {
    private lateinit var viewModel: EntriesByDateViewModel
    private var titleText: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            EntriesByDateViewModelFactory()
        ).get(EntriesByDateViewModel::class.java)
    }

    private val entriesAdapter = EntryAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_entries_by_date, container, false)
        val recycler = view.findViewById<RecyclerView>(R.id.entries_by_date_recycler)
        recycler.layoutManager = LinearLayoutManager(view.context)
        recycler.adapter = entriesAdapter
        recycler.addItemDecoration(
            DividerItemDecoration(
                recycler.context,
                (recycler.layoutManager as LinearLayoutManager).orientation
            )
        )

        viewModel.entries().observe(this, { entriesAdapter.addData(it) })
        viewModel.showError().observeEvent(this, {
            Toast.makeText(view.context, it, Toast.LENGTH_SHORT).show()
        })
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    companion object {
        @JvmStatic
        fun newInstance() = EntriesByDateFragment()

        private const val TAG = "EntriesByDateFragment"
    }
}