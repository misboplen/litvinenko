package com.bbh.devlife

import android.opengl.Visibility
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide

class EntriesRandomFragment : Fragment() {

    private lateinit var viewModel: EntriesRandomViewModel

    private lateinit var layoutStub: View
    private lateinit var layoutMain: View
    private lateinit var prevButton: Button
    private lateinit var nextButton: View
    private lateinit var tryAgainButton: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, EntriesRandomViewModelFactory())
            .get(EntriesRandomViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_random_entry, container, false)
        findViews(view)

        nextButton.setOnClickListener { viewModel.nextClicked() }
        prevButton.setOnClickListener { viewModel.prevClicked() }
        tryAgainButton.setOnClickListener { viewModel.tryAgainClicked() }

        viewModel.currentPage().observe(this) {
            prevButton.isEnabled = it != 0
        }

        viewModel.showError().observeEvent(this) { showStub() }
        viewModel.currentEntry().observe(this) {
            val entry = it
            val authorTv: TextView = view.findViewById(R.id.authorTv)
            val dateTv: TextView = view.findViewById(R.id.dateTv)
            val descriptionTv: TextView = view.findViewById(R.id.descriptionTv)
            val likesTv: TextView = view.findViewById(R.id.likesTv)
            val commentsTv: TextView = view.findViewById(R.id.commentsTv)
            val gifIv: ImageView = view.findViewById(R.id.gifView)
            val context = view.context

            authorTv.text = entry.authorName
            dateTv.text = entry.dateString
            descriptionTv.text = entry.description
            likesTv.text = entry.rating.toString()
            commentsTv.text = entry.commentsCount.toString()

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 6f
            circularProgressDrawable.centerRadius = 100f
            circularProgressDrawable.start()

            //gifIv.setImageDrawable(circularProgressDrawable)

            // TODO: remove it later:
            Log.d(TAG, "bind: glide load with url: ${entry.url}")
            Glide.with(context)
                .load(entry.url)
                //.placeholder(R.drawable.ic_baseline_ac_unit_350)
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_baseline_ac_unit_350)
                .into(gifIv)

            showMain()
        }

        showMain()
        return view
    }

    private fun findViews(view: View) {
        layoutMain = view.findViewById(R.id.entriesRandomLayout)
        layoutStub = view.findViewById(R.id.entriesRandomStub)
        nextButton = view.findViewById(R.id.nextButton)
        prevButton = view.findViewById(R.id.prevButton)
        tryAgainButton = view.findViewById(R.id.tryAgainButton)
    }

    private fun showMain() {
        layoutMain.visibility = View.VISIBLE
        layoutStub.visibility = View.GONE
    }

    private fun showStub() {
        layoutMain.visibility = View.GONE
        layoutStub.visibility = View.VISIBLE
    }

    companion object {
        fun newInstance() = EntriesRandomFragment()

        private const val TAG = "EntriesRandomFragment"
    }
}