package com.bbh.devlife.util

import androidx.lifecycle.Observer

/**
 * An [Observer] for [SingleEvent]s, simplifying the pattern of checking if the [SingleEvent]'s content has
 * already been handled.
 *
 * [onEventUnhandledContent] is *only* called if the [SingleEvent]'s contents has not been handled.
 *
 * Usage sample:
 * ```
 *   // someOneTimeEffect is wrapped in an 'Event'
 *   someViewModel.someOneTimeEffect.observe(viewLifecycleOwner, SingleEventObserver {
 *       // Do something here with 'it'.
 *   })
 * ```
 */
class SingleEventObserver<T : Any>(private val onEventUnhandledContent: (T) -> Unit) :
    Observer<SingleEvent<T>> {
    override fun onChanged(event: SingleEvent<T>?) {
        event?.getContentIfNotHandled()?.let(onEventUnhandledContent)
    }
}
