package com.bbh.devlife

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.bbh.devlife.util.SingleEvent
import com.bbh.devlife.util.SingleEventObserver

inline fun <T : Any> LiveData<SingleEvent<T>>.observeEvent(
    owner: LifecycleOwner,
    crossinline onEventUnhandledContent: (T) -> Unit
) {
    observe(owner, SingleEventObserver { onEventUnhandledContent(it) })
}