package com.bbh.devlife

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter


class EntriesFragmentStateAdapter(activity: FragmentActivity)
    : FragmentStateAdapter(activity) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> EntriesRandomFragment.newInstance()
            1 -> EntriesByDateFragment.newInstance()
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int = TAB_TITLES.size

    companion object {
        val TAB_TITLES = arrayOf(
            R.string.tab_text_2,
            R.string.tab_text_1
        )
    }
}
