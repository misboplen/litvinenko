package com.bbh.devlife

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bbh.devlife.data.*
import com.bbh.devlife.util.SingleEvent
import kotlin.math.log

class EntriesRandomViewModel(
    private val repo: EntriesRepository
) : ViewModel(), EntriesRepository.Listener {

    init {
        repo.subscribe(this)
    }

    private val _pagesList = mutableListOf<Int>()

    private val _currentPage = MutableLiveData<Int>(0)
    fun currentPage(): LiveData<Int> = _currentPage

    private val _currentEntry = MutableLiveData<Entry>()
    fun currentEntry(): LiveData<Entry> = _currentEntry

    private val _showError = MutableLiveData<SingleEvent<Int>>()
    fun showError(): LiveData<SingleEvent<Int>> = _showError

    init {
        addNewEntry()
    }

    fun nextClicked() {
        val page = _currentPage.value!!
        if (page == _pagesList.lastIndex)
        {
            addNewEntry()
        } else {
            goToEntry(page+1)
        }
    }

    fun prevClicked() {
        Log.i(TAG, "prevClicked while current page is: ${_currentPage.value}")
        val page = _currentPage.value!!
        if (_currentPage.value == 0) {
            Log.e(TAG, "prevClicked while current page is: $page")
            return
        }
        goToEntry(page-1)
    }

    fun tryAgainClicked() {
        addNewEntry()
    }

    private fun goToEntry(page: Int) {
        repo.getEntryById(_pagesList[page])
        _currentPage.value = page
    }

    private fun addNewEntry() {
        repo.getRandomEntry()
    }

    override fun onEntriesByDateLoaded(entries: RequestResult<List<Entry>>) {
        // do nothing
    }

    override fun onRandomEntryLoaded(entry: RequestResult<Entry>) {
        entry.ifSuccess {
            _pagesList.add(it.id)
            _currentPage.value = _pagesList.lastIndex
            _currentEntry.value = it
        }
        entry.ifFailure { processFailureResult(it) }
    }

    override fun onEntryByIdLoaded(entry: RequestResult<Entry>) {
        entry.ifSuccess {
            _currentPage.value = _pagesList.indexOf(it.id)
            _currentEntry.value = it
        }
        entry.ifFailure { processFailureResult(it) }
    }

    private fun processFailureResult(failed: RequestResult<*>) {
        when(failed) {
            NetworkFailure -> _showError.value = SingleEvent(R.string.network_err_msg)
            DataFailure -> _showError.value = SingleEvent(R.string.unsuccessful_response_err_msg)
            is Failure -> _showError.value = SingleEvent(R.string.unknown_request_error)
            is Success -> TODO()
        }
    }


    companion object {
        private const val TAG = "EntriesRandomViewModel"
    }
}