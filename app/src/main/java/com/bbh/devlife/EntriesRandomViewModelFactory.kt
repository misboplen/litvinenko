package com.bbh.devlife

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bbh.devlife.data.EntriesRepositoryProvider

class EntriesRandomViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        if (modelClass.isAssignableFrom(EntriesRandomViewModel::class.java))
            @Suppress("UNCHECKED_CAST")
            EntriesRandomViewModel(EntriesRepositoryProvider.getRepository()) as T
        else
            throw IllegalArgumentException("Unknown class name")
}
