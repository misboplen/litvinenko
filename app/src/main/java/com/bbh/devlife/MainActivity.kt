package com.bbh.devlife

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val entriesTabPager: ViewPager2 = findViewById(R.id.entries_tab_pager)
        entriesTabPager.adapter = EntriesFragmentStateAdapter(this)

        val tabs = findViewById<TabLayout>(R.id.entries_by_tab)
        TabLayoutMediator(tabs, entriesTabPager) { tab, position ->
            tab.text = getString(EntriesFragmentStateAdapter.TAB_TITLES[position])
        }.attach()
    }
}