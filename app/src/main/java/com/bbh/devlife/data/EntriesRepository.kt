package com.bbh.devlife.data

interface EntriesRepository {

    fun getEntriesByDate(page: Int = 0)
    fun getRandomEntry()
    fun getEntryById(id: Int)

    fun subscribe(listener: Listener)
    fun unsubscribe(listener: Listener)

    interface Listener {
        fun onEntriesByDateLoaded(entries: RequestResult<List<Entry>>)
        fun onRandomEntryLoaded(entry: RequestResult<Entry>)
        fun onEntryByIdLoaded(entry: RequestResult<Entry>)
    }
}