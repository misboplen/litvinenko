package com.bbh.devlife.data

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EntriesNetworkRepository(private val api: DevLifeApi): EntriesSubscribingRepository() {
    override fun getEntriesByDate(page: Int) {
        val call = api.getEntriesByDate()
        call.enqueue(EntriesByDateCallback())
    }

    override fun getRandomEntry() {
        val call = api.getRandomEntry()
        call.enqueue(RandomEntryCallback())
    }

    override fun getEntryById(id: Int) {
        TODO("not implemented")
    }

    inner class EntriesByDateCallback : Callback<DevLifeResponse> {
        override fun onFailure(call: Call<DevLifeResponse>, t: Throwable) {
            Log.e(TAG, "call failed with: $t")
            listeners.onEntriesByDateLoaded(NetworkFailure)
        }

        override fun onResponse(
            call: Call<DevLifeResponse>,
            response: Response<DevLifeResponse>
        ) {
            if (!response.isSuccessful || response.body() == null) {
                Log.e(TAG, "response is not successful, code: ${response.code()}")
                listeners.onEntriesByDateLoaded(DataFailure)
                return
            }

            val result = response.body()?.result?.toEntries()
            listeners.onEntriesByDateLoaded(if (result != null) Success(result) else DataFailure)
        }
    }

    inner class RandomEntryCallback : Callback<EntryJson> {
        override fun onFailure(call: Call<EntryJson>, t: Throwable) {
            Log.e(TAG, "call failed with: $t")
            listeners.onRandomEntryLoaded(NetworkFailure)
        }

        override fun onResponse(
            call: Call<EntryJson>,
            response: Response<EntryJson>
        ) {
            if (!response.isSuccessful || response.body() == null) {
                Log.e(TAG, "response is not successful, code: ${response.code()}")
                listeners.onRandomEntryLoaded(DataFailure)
                return
            }

            val result = response.body()?.toEntry()
            listeners.onRandomEntryLoaded(if (result != null) Success(result) else DataFailure)
        }
    }

    companion object {
        private const val TAG = "EntriesNetworkRepository"
    }
}