package com.bbh.devlife.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EntryJson(
    @SerializedName("id")
    @Expose
    val id: Int,

    @SerializedName("description")
    @Expose
    val description: String,

    @SerializedName("votes")
    @Expose
    val votes: Int,

    @SerializedName("author")
    @Expose
    val author: String,

    @SerializedName("date")
    @Expose
    val date: String, // TODO: consider using DAteTime or smth

    @SerializedName("gifURL")
    @Expose
    val gifURL: String,

    @SerializedName("gifSize")
    @Expose
    val gifSize: Int,

    @SerializedName("previewURL")
    @Expose
    val previewURL: String,

    @SerializedName("videoURL")
    @Expose
    val videoURL: String,

    @SerializedName("videoPath")
    @Expose
    val videoPath: String,

    @SerializedName("videoSize")
    @Expose
    val videoSize: Int,

    @SerializedName("type")
    @Expose
    val type: String, // TODO: consider using enum

    @SerializedName("width")
    @Expose
    val width: Int,

    @SerializedName("height")
    @Expose
    val height: Int,

    @SerializedName("commentsCount")
    @Expose
    val commentsCount: Int,

    @SerializedName("fileSize")
    @Expose
    val fileSize: Int,

    @SerializedName("canVote")
    @Expose
    val canVote: Boolean
)

data class DevLifeResponse(
    @SerializedName("result")
    val result: List<EntryJson>
)