package com.bbh.devlife.data

class EntriesRepositoryImpl(private val netRepo: EntriesNetworkRepository) : EntriesSubscribingRepository() {
    init {
        netRepo.subscribe(this.Interceptor())
        netRepo.subscribe(listeners)
    }

    private val _cache: MutableMap<Int, Entry> = mutableMapOf()

    override fun getEntriesByDate(page: Int) {
        netRepo.getEntriesByDate(page)
    }

    override fun getRandomEntry() {
        netRepo.getRandomEntry()
    }

    override fun getEntryById(id: Int) {
        val cached = _cache[id]
        if (cached != null){
            listeners.onEntryByIdLoaded(Success(cached))
        } else {
            netRepo.getEntryById(id)
        }
    }

    private inner class Interceptor: EntriesRepository.Listener {
        override fun onEntriesByDateLoaded(entries: RequestResult<List<Entry>>) {
            entries.ifSuccess { list ->
                list.associateByTo(_cache) { it.id }
            }
        }

        override fun onRandomEntryLoaded(entry: RequestResult<Entry>) {
            entry.ifSuccess {
                _cache.put(it.id, it)
            }
        }

        override fun onEntryByIdLoaded(entry: RequestResult<Entry>) {
            entry.ifSuccess {
                _cache.put(it.id, it)
            }
        }
    }

    companion object {
        private const val TAG = "EntriesRepositoryImpl"
    }
}