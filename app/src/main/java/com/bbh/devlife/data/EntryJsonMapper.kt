package com.bbh.devlife.data

// TODO: here comes NPE on id: 17074
fun EntryJson.toEntry(): Entry = Entry(
    id,
    author,
    date,
    description,
    gifURL,
    commentsCount,
    votes,
)

fun List<EntryJson>.toEntries(): List<Entry> = this.map { it.toEntry() }

fun DevLifeResponse.toEntriesList(): List<Entry> = result.map { it.toEntry() }