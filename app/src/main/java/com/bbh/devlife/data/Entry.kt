package com.bbh.devlife.data

data class Entry(
    val id: Int,
    val authorName: String,
    val dateString: String,
    val description: String,
    val url: String?,
    val commentsCount: Int,
    val rating: Int
)