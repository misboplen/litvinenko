package com.bbh.devlife.data

import com.bbh.devlife.data.DevLifeResponse
import com.bbh.devlife.data.EntryJson
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface DevLifeApi {
    @GET("/latest/{page}?json=true")
    fun getEntriesByDate(@Path("page") page: Int = 0): Call<DevLifeResponse>

    @GET("/random?json=true")
    fun getRandomEntry(): Call<EntryJson>
}
