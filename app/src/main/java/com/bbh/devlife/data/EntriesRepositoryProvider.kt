package com.bbh.devlife.data

object EntriesRepositoryProvider {
    private val repository =
        EntriesRepositoryImpl(EntriesNetworkRepository(DevLifeNetworkService.getApi()))

    fun getRepository(): EntriesRepository = repository
}