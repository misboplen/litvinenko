package com.bbh.devlife.data

abstract class EntriesSubscribingRepository : EntriesRepository {
    private val _listeners = SubscribingListeners()
    protected val listeners: EntriesRepository.Listener = _listeners

    override fun subscribe(listener: EntriesRepository.Listener) {
        _listeners.subscribe(listener)
    }

    override fun unsubscribe(listener: EntriesRepository.Listener) {
        _listeners.unsubscribe(listener)
    }

    private class SubscribingListeners : EntriesRepository.Listener {
        private val listeners = mutableListOf<EntriesRepository.Listener>()

        fun subscribe(listener: EntriesRepository.Listener) {
            if (listeners.contains(listener)) return
            listeners.add(listener)
        }

        fun unsubscribe(listener: EntriesRepository.Listener) {
            listeners.remove(listener)
        }

        override fun onEntriesByDateLoaded(entries: RequestResult<List<Entry>>) {
            listeners.forEach { it.onEntriesByDateLoaded(entries) }
        }

        override fun onRandomEntryLoaded(entry: RequestResult<Entry>) {
            listeners.forEach { it.onRandomEntryLoaded(entry) }
        }

        override fun onEntryByIdLoaded(entry: RequestResult<Entry>) {
            listeners.forEach { it.onEntryByIdLoaded(entry) }
        }
    }
}