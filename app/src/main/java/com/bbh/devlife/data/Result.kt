package com.bbh.devlife.data

import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

sealed class RequestResult<out T>

data class Success<out R>(val value: R): RequestResult<R>()

object NetworkFailure : RequestResult<Nothing>()
object DataFailure : RequestResult<Nothing>()

data class Failure(val throwable: Throwable?) : RequestResult<Nothing>()

inline fun <T, R> RequestResult<T>.ifSuccess(block: (T) -> R): R? =
    if (this is Success) block(this.value) else null

inline fun <T, R> RequestResult<T>.ifFailure(block: (RequestResult<T>) -> R): R? =
    if (this !is Success) block(this) else null