package com.bbh.devlife

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bbh.devlife.data.EntriesRepositoryProvider

class EntriesByDateViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        if (modelClass.isAssignableFrom(EntriesByDateViewModel::class.java))
            @Suppress("UNCHECKED_CAST")
            EntriesByDateViewModel(EntriesRepositoryProvider.getRepository()) as T
        else
            throw IllegalArgumentException("Unknown class name")
}
