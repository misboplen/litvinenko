package com.bbh.devlife

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bbh.devlife.data.*
import com.bbh.devlife.util.SingleEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EntriesByDateViewModel(private val repo: EntriesRepository) : ViewModel(),
    EntriesRepository.Listener {

    private val _entries = MutableLiveData<List<Entry>>()
    fun entries(): LiveData<List<Entry>> = _entries

    private val _showError = MutableLiveData<SingleEvent<Int>>()
    fun showError(): LiveData<SingleEvent<Int>> = _showError

    init {
        @Suppress("UsePropertyAccessSyntax")

        repo.subscribe(this)

        // repo.getEntriesByDate()
    }

    override fun onEntriesByDateLoaded(entries: RequestResult<List<Entry>>) {
        when (entries) {
            is Success -> _entries.value = entries.value
            NetworkFailure -> _showError.value = SingleEvent(R.string.network_err_msg)
            DataFailure -> _showError.value = SingleEvent(R.string.unsuccessful_response_err_msg)
            is Failure -> _showError.value = SingleEvent(R.string.unknown_request_error)
        }
    }

    override fun onRandomEntryLoaded(entry: RequestResult<Entry>) {
        // do nothing
    }

    override fun onEntryByIdLoaded(entry: RequestResult<Entry>) {
        // do nothing
    }

    companion object {
        private const val TAG = "EntriesByDateViewModel"
    }
}