package com.bbh.devlife

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bbh.devlife.data.Entry
import com.bumptech.glide.Glide

class EntryAdapter() : RecyclerView.Adapter<EntryAdapter.EntryViewHolder>() {
    private val entries = mutableListOf<Entry>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_entry, parent, false)
        return EntryViewHolder(view)
    }

    override fun onBindViewHolder(holder: EntryViewHolder, position: Int) =
        holder.bind(entries[position])

    override fun getItemCount(): Int = entries.count()

    fun addData(newEntries: List<Entry>) {
        entries.addAll(newEntries)
        notifyDataSetChanged()
    }

    class EntryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val _authorTv: TextView = view.findViewById(R.id.authorTv)
        private val _dateTv: TextView = view.findViewById(R.id.dateTv)
        private val _descriptionTv: TextView = view.findViewById(R.id.descriptionTv)
        private val _likesTv: TextView = view.findViewById(R.id.likesTv)
        private val _commentsTv: TextView = view.findViewById(R.id.commentsTv)
        private val _gifIv: ImageView = view.findViewById(R.id.gifView)
        private val _context = view.context

        fun bind(entry: Entry) {
            _authorTv.text = entry.authorName
            _dateTv.text = entry.dateString
            _descriptionTv.text = entry.description
            _likesTv.text = entry.rating.toString()
            _commentsTv.text = entry.commentsCount.toString()

            val circularProgressDrawable = CircularProgressDrawable(_context)
            circularProgressDrawable.strokeWidth = 6f
            circularProgressDrawable.centerRadius = 100f
            circularProgressDrawable.start()

            Log.d(TAG, "bind: glide load with url: ${entry.url}")
            Glide.with(_context)
                .load(entry.url)
                .placeholder(R.drawable.ic_baseline_ac_unit_350)
                .error(R.drawable.ic_baseline_ac_unit_350)
                .into(_gifIv)
        }

        companion object {
            private const val TAG = "EntryViewHolder"
        }
    }
}